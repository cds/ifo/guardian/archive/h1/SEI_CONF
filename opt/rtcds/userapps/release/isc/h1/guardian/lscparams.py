#
# $Id$
# $HeadURL$

import numpy


# If you change these input powers, you *must* also reload the LASER_PWR guardian
# before doing an initial alignment.
# The LASER_PWR guardian creates fast states based on these numbers, and ALIGN_IFO guardian tries to
# go to those fast states.  So, ALIGN_IFO guardian will fail if the other guardian LASER_PWR
# states don't exist yet.
# Also, the IMC_LOCK guardian depends on the NLN number, so make sure to reload IMC_LOCK if you change the NLN number.
input_power = {'PRXY': 2,
               'MICH': 10,
               'SRXY': 10,
               'POWER_10W':  10,
               'POWER_25W':  25,
               'NLN': 60,
               }
# normally MICH=10, SRXY=10, but lowering for time when HAM6 in air
manual_control = False #we want to control things more manually, not to be used for unattended locking but useful when we are making changes and commissoning sometimes
if manual_control: 
    #this also changes weights in ISC_LOCK to go to green arms manual (so not to do increase flashes)
    auto_MICH_fringes = False #False if you do not want the guardian to automaticaly do MICH fringesss
    auto_PRMI = False    
else:
    auto_MICH_fringes = True  #False if you do not want the guardian to automaticaly do MICH fringesss
    auto_PRMI = True 
# Reload CO2 guardians when changing this
tcs_nom_annular_pwr = {'X': 1.7,
                       'Y': 1.7 
                       }

# (sdf model name, dcuid, userapps area)
sdf_revert_models = [('alsex', 85, 'als'),
                     ('alsey', 95, 'als'),
                     ('asc', 19, 'asc'),
                     ('ascimc', 20, 'asc'),
                     ('iscex', 86, 'isc'),
                     ('iscey', 96, 'isc'),
                     ('lsc', 10, 'lsc'),
                     ('omc', 8, 'omc'),
                     #('pslfss', 80, 'psl'),
                     #('psliss', 79, 'psl'),
                     #('pslpmc', 81, 'psl'),
                     ('susbs', 31, 'sus'),
                     ('susetmx', 88, 'sus'),
                     ('susetmy', 98, 'sus'),
                     ('sushtts', 21, 'sus'),
                     ('susifoout', 46, 'sus'),
                     ('sussqzout', 22, 'sus'),
                     ('sussqzin', 162, 'sus'),
                     ('susfc1', 161, 'sus'),
                     ('susfc2', 168, 'sus'),
                     ('susitmx', 29, 'sus'),
                     ('susitmy', 30, 'sus'),
                     ('susim', 104, 'sus'),
                     ('susmc1', 34, 'sus'),
                     ('susmc2', 39, 'sus'),
                     ('susmc3', 35, 'sus'),
                     ('susprm', 36, 'sus'),
                     ('suspr2', 40, 'sus'),
                     ('suspr3', 37, 'sus'),
                     ('sussrm', 45, 'sus'),
                     ('sussr2', 41, 'sus'),
                     ('sussr3', 44, 'sus'),
                     ('sustmsx', 89, 'sus'),
                     ('sustmsy', 99, 'sus'),
                     ('sysexisc', 1028, 'sys'),
                     ('syseyisc', 1031, 'sys')]

### ISS Final Gain Value
ISS_acquisition_gain = -2 # RWS changed back to -2 from -5 after going back to 60W
ISS_FinalGain = 2 # 60 W, alog 70895, JCD 27June2023


### CARM Gain sliders initial values for the beginning of locking
# IMC
IMC_FASTGAIN_PowerUp_Scaler = 6 # CC: Value we want on IMC_FASTGAIN when at 2 watts, IMC_FASTGAIN is controlled by ISC_library.IMC_power_adjust()
IMC_IN1_gain = -1  #SED trying reset IMC ugf to what was measured in 65676 # -20dB   JCD 7May2019
IMC_MCL_gain = 1
# COMM
COMM_IMC_IN2GAIN = -12 # CC: Jan 23, 2019, defining new initial COMM gain here
COMM_LSC_IN2GAIN = -32
COMM_LSC_FASTGAIN = 7
COMM_HANDOFF_PART2_LSC_IN2GAIN = 17
# PREP_TR_CARM
#LSC_REFLBIAS_GAIN_PREP_TR_CARM = 50.4   #moved to carm offset dict
LSC_REFL_SUM_A_IN1GAIN_PREP_TR_CARM = 0
LSC_REFL_SERVO_IN1GAIN_PREP_TR_CARM = -16
# START_TR_CARM
ALS_C_REFL_DC_BIAS_GAIN_START_TR_CARM = 24
# CARM_TO_TR
# CARM_150_PICOMETERS
#ALS_C_REFL_DC_BIAS_GAIN_CARM_150_PICOMETERS = 39  #moved to carm offset dict
# CARM_OFFSET_REDUCTION
LSC_REFLBIAS_GAIN_CARM_OFFSET_REDUCTION = 86
# CARM_TO_REFL
LSC_TR_REFLAIR9_CARM_TO_REFL = -0.8

#these are parameters that are used in CARM offset reduction, collecting them here so that when we have a bad recycling gain we can preseve the old settings but use some temporary ones
'''carm_offset ={'TR_CARM_offsets':
                {'RF_DARM': -5,
                'DARM_boost':-20,
                'carmH0_OFSref':-40
                'als_c_comm_vco_freq': -200,
                'carm_to_tr': -0.8,},
              'gains':
                {'LSC_REFLBIAS_PREP_TR_CARM':50.4,
                 'ALS-C_REFLBIAS_150PM':39,
                 'ALS_C_DIFF_PLL_servo': 0.01,
                 'RF_DAMR_intrix':-1e-6,
                 'LSC_REFLBIAS_CARM_OFFSET_REDUCTION':86},
             }'''
#October 2019 terrible recycling gain relocking
carm_offset ={'TR_CARM_offsets':
                {'RF_DARM': -3,
                 'DARM_boost': -12,
                 'carmH0_OFSref': -40,
                 'als_c_comm_vco_freq': -200,
                 'carm_to_tr': -0.8,},
              'gains':
                {'LSC_REFLBIAS_PREP_TR_CARM':50.4,
                 'ALS-C_REFLBIAS_150PM':39,
                 'ALS_C_DIFF_PLL_servo': 0.01,
                 'RF_DARM_intrix':-1e-6, 
                 'LSC_REFLBIAS_CARM_OFFSET_REDUCTION':86},
             }

# parameters related to CARM (offset reduction) sequence
carmHO_OFSref      =-40   # reference OFFSET for checking REFL_DSC
carmHO_PrefFraction= 0.57 # from alog 43346, REFL_DC fraction at OFFSET = -40 (arm transmission 800)
carmHO_OFSideal    =-50   # was -54 this is the target corresponding to about 85% of arm buildup for handoff to RF.
# this was -50 until 2/3/2023, trying 
# Defining omc_sign for DARM_OFFSET state
omc_sign = 1 # ddb trying other side of DARM fringe, change from 1 to -1 03/20/2023

# Modulation depths (in slider units)
mod_depth = {
    '9':  23.4,
    '45': 27.0,
}

gate_valve_flag = False # False means arm gate valves open

# Which ESD to use for ALS locking?
ALS_ESD = 'X'

ETMX_ESD_LOWNOISE_BIAS = -1 # -1 means -400 Volts
ETMX_GND_MIN_BIAS = 3.25 # should be +128 V
ETMX_GND_MIN_DriveAlign_gain = 198.6643 # This drivealign gain keeps DARM olg constant when bias goes down # 20240831 Changed by Francisco's script
ETMY_GND_MIN_BIAS = -4.9 # should be + 115V alog 67075, bias set to minimize coupling of noise on ground to DARM. 


# If True, bypass the closing of ALS shutters.  If False, park the VCO and shut shutters.
#see alog 72522, instead of using this flag edit weight in ISC_LOCK edges. 
#leaveGreenOpen = False

# When transitioning lownoise ESD, stay on split actuator or go all the way back to ETMX?
use_EX_L1L2 = True
useNewDARM = True


gain = {'MICH_DARK':
                {'ACQUIRE':4000,
                 'LOCKED': 8000}, # was 4000 for ACQ, 8000 for LOCKED, changed temporarily by SED March 14 2021
        'MICH_BRIGHT':
                {'ACQUIRE':-4000,
                 'LOCKED': -8000}, #was 2000 for acq and 1000 for locked, changed GLM Sept 13 2018
        'PRXY':-500000, # Was changed sometime btwn April 5-23 2019 from -3200 to -525000.  Lowering to -520000 to not rail PRM JCD 5Aug2019
        'SRXY':78000, # changed from 10000, SED CRC July 22 2018, back to 10000 SB Aug 25 2018
        'XARM_IR':0.04,
        'YARM_IR':0.04,
        'DRMI_MICH':{'ACQUIRE': 1.2, # 2018-11-28 TVo, SED from 2.8 as a guess from estimating the PRMI OLTF
                     'NO_ARMS': 3.0,  # 2018-11-26 TVo changed to 3.0 to match the OLG
                     'W_ALS': 2.8,  # was 1.4, increased July 28 2018, was 1.4 - now in FM2
                     '3F_135': 1.115, # 2.23 #changed from 1.25 SED CRC June 21 2018
                     '3F_27I': -0.674, # -1.348
                     '1F_45': 1.824 *2, # multiplied for whitneing gain change alog 44420 mulitplied by 2 for HAM1 BS change
                     '1F_9': 0.0, 
                     'AIR_1F_45':0.95}, 
        'DRMI_PRCL':{'ACQUIRE':12, # was 12 # was 16, changed 10Sept2018 JCD, was 12 05/19
                     'NO_ARMS':18,
                     'W_ALS': 8,  # was 8, changed July 28 2018
                     '3F_27':  -1.22, # was -2.43
                     '1F_9': 0.037 * 2, #factor of 2 for HAM1 BS change 
                     'AIR_1F_9':0.7},  
        'DRMI_SRCL':{'ACQUIRE':-30,
                     'NO_ARMS':-33,
                     'W_ALS':  -18,
                     '3F_27':  2.124,
                     '3F_135':  1.7,
                     '1F_9':  0.10408, #re-tuned after phasing POP9, SEpt 23rd 2024 # Tuned 28Jun2023, JCD, alog 70919
                     '1F_45': 1.856 * 2, #factor of 2 for HAM1 BS change
                     'AIR_1F_9':0.45,
                     'AIR_1F_45':0.81}, 
        'PRMI_MICH':{'W_ALS':2.0,     # Changed from 2.8, measured OLG TVo, SED 20181128, was 3.2 05/19 RC
                     'NO_ARMS':2.6},   # changed from 2.8, SED CRC June 24 2018
        'PRMI_PRCL':{'W_ALS':9,     # 12 is too high.  JCD 10Sept2018, was 6 05/19 RC
                     'NO_ARMS':10, # Changed from 16, measured OLG TVo, SED 20181128
                     'CARRIER':-30},
        'MICHFF':  1,
        'SRCLFF1':  1, 
        'SRCLFF2':  0,
        'PRCLFF': 0.6,
        'ALS_ASC_DOF3': {'X': 0.1, 'Y': 0.03},
        }

asc_gains = {'DHARD': {'P':{'DHARD_WFS_initial':-1,
                            'DHARD_WFS_final':-10,
                            'RESONANCE':-30,  #Feb 2 2023, gain reduced from -30 after RH change
                            'ENGAGE_ASC_FOR_FULL_IFO':-30, #02072022 GLM lowered from -40 to prevent ringups
                            'ENGAGE_SOFT_LOOPS': -40, # EMC increased from -40, see alog 63817
                            'LOWNOISE_ASC':-30,              # Seems a bit too high with -42, so back to -30 to reduce 4.5 Hz buzzing. JCD 14Mar2019.[Was -30, with new 1Hz resG need -42 JCD 6Feb2019]
                            },
                        'Y':{'DHARD_WFS_initial':-1, 
                            'DHARD_WFS_final':-15,
                            'RESONANCE':-40,
                            'ENGAGE_ASC_FOR_FULL_IFO':-40, #EMC increased from -30 July 7 2022
                            'LOWNOISE_ASC':-40,
                            },
                       },#close DHARD
                'CHARD':{'P':{'ENGAGE_ASC_FOR_FULL_IFO':1.7, # 1.7 was better than 2 after phasing REFL45 JCD 26Apr2021
                              'LOWNOISE_ASC':1,
                             },
                        'Y':{'ENGAGE_ASC_FOR_FULL_IFO_initial':0.5,
                              'ENGAGE_ASC_FOR_FULL_IFO_increase':17.5, # old value: 2.5,
                              'ENGAGE_ASC_FOR_FULL_IFO_final':175, # old value: 25,
                              'LOWNOISE_ASC':125, # best gain to reduce 6 Hz oscillations during thermalization
                              '50W':10, # 47 W for stricter cutoff 18, 05May2021, JCD&VS
                             },
                        },#close CHARD
                'CSOFT':{'P':{'ENGAGE_SOFT_LOOPS':20, #was 30, changed by EMC 20220808 to avoid 3.3 Hz ringup, was 15 changed 20210502 GLM
                              'LOWNOISE_ASC':15, # alog 66415 EMC changed to 20 20220713, was 10, old comment: alog 46975
                             },#close csoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #CSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':3, #probably unnecessary to make such a small gain adjustment
                              },#close csoft y
                            },#close csoft
                'DSOFT':{'P':{'ENGAGE_SOFT_LOOPS':10, # was 10, changed by EMC for new intrix
                              'LOWNOISE_ASC':20,      # was 10, changed for new intrix # was 30 20190207, double gain to help suppress 0.47Hz osc. JCD 1Feb2019
                             },#close dsoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #DSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':4, #probably unnecessary to make such a small gain adjustment
                              },#close dsoft y
                            },#close dsoft
                'MICH':{'P':{'ENGAGE_DRMI_ASC':-0.25,
                             'ENGAGE_PRMI_ASC':-0.2,
                            },
                        'Y':{'ENGAGE_DRMI_ASC':-0.2,
                             'ENGAGE_PRMI_ASC':-0.2,
                            },
                        },#close MICH
                'RPC':{'DHARD_P':{'82':0,      # If less than 65kW arm circulating power
                                  '113':-0.4,   # If less than 90kW, greater than 65kW
                                  '126':-0.8,
                                  '138.6':-0.9,
                                  '164':-1.0, 
                                  '176':-1.1,
                                  '195':  -1.2,
                                  '214':-1.45,
                                  '233':  -1.6,
                                  '258':  -2.6,   # New val, alog 58818 JCD 3May2021
                                  '330': -2.6,
                                  '352':  -2.9, # New val, EMC, 20220830
                                  '365': -2.9,
                                  '400': -3.1,
                            }, #close DHARD RPC P
                       'DHARD_Y':{'82':0,
                                  '113':-0.4,
                                  '126':-0.8,
                                  '138.6':-0.9,
                                  '164':-1.0, 
                                  '176':-1.1,
                                  '195':-1.2,
                                  '214':-1.45,
                                  '233':-1.6,
                                  '258':-1.6,
                                  '330':-1.6,
                                  '352': -2.0,  # new val EMC
                                  '365': -2.0,
                                  '400': -2.0,
                            }, #close RPC DHARD Y
                       'CHARD_P':{'82':0,
                                  '113':0.4,
                                  '126':0.8,
                                  '138.6':0.9,
                                  '164':2.3,     # New val, alog 58818 JCD 3May2021
                                  '176':2.3,
                                  '195':2.3,
                                  '214':2.3,
                                  '233':2.3,
                                  '258':5.0,       # New val, alog 58818 JCD 3May2021
                                  '330':5.0,
                                  '352': 5.0,       # New val, EMC, 20220830
                                  '365': 6.0,
                                  '400': 6.0,
                                }, #close RPC CHARD P
                       'CHARD_Y':{'82':0,
                                  '113':0.6,
                                  '126':1.2,
                                  '138.6':1.3,
                                  '164':1.5,  
                                  '176':1.6,
                                  '195':1.8,
                                  '214':2,
                                  '233':2.2,
                                  '258':2.5,
                                  '330':3,
                                  '352':3.5,  # new val EMC
                                  '365':3.5,
                                  '400':3.8,
                                }, #close  CHARD Y RPC
                       'CSOFT_P':{'82':0,
                                  '113':0,
                                  '126':0,
                                  '138.6':1,
                                  '164':1.5,
                                  '176':1.5,
                                  '195':1.5,
                                  '214':1.5,
                                  '233':1.5,
                                  '258': 1.5, # new val added by EMC
                                  '330': 2.0,
                                  '352':4.2, # new val added by EMC
                                  '365':4.8,
                                  '400':4.8,
                                }, #close csoft RPC P
                       'DSOFT_P':{'82':0,
                                  '113':0,
                                  '126':0,
                                  '138.6':0,
                                  '164':1,
                                  '176':1,
                                  '195':1,
                                  '214':1,
                                  '233':1,
                                  '258': 1,
                                  '330':1,
                                  '352':3,
                                  '365':3.5,
                                  '400':3.5,
                                }, #close dsoft RPC P
                        'CSOFT_Y':{'82':0,
                                  '113':0,
                                  '126':0,
                                  '138.6':1,
                                  '164':1.5,
                                  '176':1.5,
                                  '195':1.5,
                                  '214':1.5,
                                  '233':1.5,
                                  '258': 1.5, # new val added by EMC
                                  '330': 1.5,
                                  '352':2, # new val added by EMC
                                  '365':2,
                                  '400':2,
                                }, #close csoft RPC Y
                        'DSOFT_Y':{'82':0,
                                  '113':0,
                                  '126':0,
                                  '138.6':0,
                                  '164':1,
                                  '176':1,
                                  '195':1,
                                  '214':1,
                                  '233':1,
                                  '258': 1,
                                  '330':1,
                                  '352':3.0,
                                  '365':3,
                                  '400':3,
                                }, #close dsoft RPC Y
                        },# close RPC
            }#close ASC gains

# gains for use in NLN for ADS and Camera_Servo. intermediate ADS gains hard-coded in ISC_LOCK. JCD 8Mar2023
ads_camera_gains = {'ADS_PIT3': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ITM spot -> PRM
                    'ADS_PIT4': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ETMX spot -> Xsoft 
                    'ADS_PIT5': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ETMY spot -> Ysoft
                    'ADS_YAW3': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ITM spot -> PRM
                    'ADS_YAW4': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ETMX spot -> Xsoft
                    'ADS_YAW5': {'gain': 20, 'max_gain': 60, 'clk': 30},  # ETMY spot -> Ysoft
                    'CAM_PIT1': {'gain': 200},  # BS spot -> PRM
                    'CAM_PIT2': {'gain': -300},  # ETMX spot -> Xsoft
                    'CAM_PIT3': {'gain': -400},  # ETMY spot -> Ysoft
                    'CAM_YAW1': {'gain': 100},  # BS spot -> PRM
                    'CAM_YAW2': {'gain': 100},  # ETMX spot -> Xsoft
                    'CAM_YAW3': {'gain': -100},  # ETMY spot -> Ysoft
                     }
                     


# filters to use when acquiring
filtmods = {'MICH':   ['FM5'],
            'PRCL':   ['FM4', 'FM9', 'FM10'],
            'SRCL':   ['FM9', 'FM10'],
            'ARM_IR': ['FM3', 'FM4', 'FM6'],
            'PRM_M2': ['FM1', 'FM3', 'FM4', 'FM6', 'FM10'],
            'SRM_M1': ['FM2', 'FM4', 'FM6', 'FM8', 'FM10'],
            }

sus_crossover = {'PRM_M1': -0.02,
                 'PRM_M2': 1,
                 'SRM_M1': -0.02} # back to -0.02 after crossover change (GV 2023-04-21) # changed from -0.02, which produced too much ringing in M1_LF


# DC readout parameters
dc_readout ={
    'DCPD_SUM_TARG': 20, # Used for acquisition, until at full power
    'DCPD_SUM_NLN': 40,  # 40mA is about 14pm with 38W injected, JCD SED 20Feb2020
    'PREF':          1.6868,
    'ERR_GAIN':     -2.9648e-6,
    'x0':            42*.73714,
    'sign':          omc_sign, # ddb, changed this from 1 to omc_sign on 03/20/2023 to reduce how many places a sign needs to be changed
}


##############################
#### A2L gains for spot position setting
##############################
# To navigate spot positions from nominally centered on the optic to desired full power position
# Added "plus 1.0" to ITMX, ITMY, and ETMY to compensate for for PUM A2L MECH being on, see LHO aLOG 63085. J. Kissel 2022-05-12
a2l_gains={
        'FULL_POWER':{
            'P2L':{'ITMX': -1.8+1.0, 
                   'ITMY':0, # JCD tuned 21Dec2023  # GV retuned July 21, 2023, was -0.1# EMC retuned for 60W June 28
                   'ETMX':4.0,
                   'ETMY':3.6+1.0},
            'Y2L':{'ITMX':2.1,     
                   'ITMY': -1.9, 
                   'ETMX':4.4,
                   'ETMY':2.2+1.0},
                    },
        # 'FULL_POWER':{ # Dan and Evans crazy spot idea
        #     'P2L':{'ITMX':0.85, 
        #            'ITMY':0.85, 
        #            'ETMX':1,
        #            'ETMY':4.6},
        #     'Y2L':{'ITMX':0,
        #            'ITMY':0,
        #            'ETMX':1.1,
        #            'ETMY':3.2},
        #             },
        'CENTER':{
            'P2L':{'ITMX':0.85, #+1.0,
                   'ITMY':0.85, #+1.0,
                   'ETMX':0.85,
                   'ETMY':0.85}, #+1.0},
            'Y2L':{'ITMX':0, #+1.0,
                   'ITMY':0, #+1.0,
                   'ETMX':0,
                   'ETMY':0 },#+1.0},# Centered on the optic #removed offsets from EY IX IY 220705 gm jd ec
                   },
        'FINAL':{
            'P2L':{'ITMX':-0.66, 
                   'ITMY':-0.05, 
                   'ETMX':3.12,
                   'ETMY':5.03}, 
            'Y2L':{'ITMX':2.980, 
                   'ITMY':-2.52, #+1.0,
                   'ETMX':4.99,
                   'ETMY':1.34 },#+1.0},# Centered on the optic #removed offsets from EY IX IY 220705 gm jd ec
                    }#spots that are the center of the optic
            }#close a2l gains dict
            
cam_offsets = {'PIT':{'1':-230,
            '2':-173,
            '3':-230},
        'YAW':{
             '1':-236,
             '2':-422,
             '3':-349.5},}

misalign_offsets = {'ETM': {'X': {'P': 10.15,
                                  'Y': -8.45},
                            'Y': {'P': 10.15,
                                  'Y': 8.45},
                            },#end ETMs
                    'TMS': {'X': {'P': -8.6,
                                  'Y': 31.6},
                            'Y': {'P': 40.1,
                                  'Y': 34.6}
                            }
                    }#end misalignment test offsets

cal_line_gains = {'ETMX_L1': 15, # for CLK, SIN, COS
                  'ETMX_L2': 8,  
                  'ETMX_L3': 0.1 
                  } # Adjusted 2024-05-23 per recommendations from LHO:77993 

# O2 values
#dc_readout ={
#    'DCPD_SUM_TARG': 20,
#    'PREF':          1.3689,
#    'ERR_GAIN':     -8.7614e-7,
#    'x0':            15.233,
#    'sign':          1,
#}


# trigger thresholds  # for several changes for DRMI see alog 44348
thresh = {'SRXY':                {'ON':0.0055,  'OFF':0.0035},
          'ARM_IR':              {'ON':0.2,  'OFF':0.05},
          'ARM_IR_FMs':          {'ON':0.25, 'OFF':0.2},
          'DRMI_MICH_noarms':    {'ON':20,   'OFF':5},
          'DRMI_MICH_als':       {'ON':37,   'OFF':18}, #20241217 TJS SED put back the 37&18 from 20&10 #Old values 37 and 18 lowered for H1 locking issues Jun2024 cg
          'DRMI_MICH_FMs':       {'ON':37,   'OFF':18}, #20241217 TJS SED put back the 37&18 from 20&10 #KK SED CG lowered from 37 on June 7 2024
          'DRMI_MICH_FMs_noarms':{'ON':24,   'OFF':5}, #added TVo DDB GLM 20181125
          'PRMIsb_MICH':         {'ON':8,   'OFF':5}, # Old Values 8 and 5. Lowered for low power PRMI locking, CRC July 23, 2018
          'PRMIcar_MICH':        {'ON':50,  'OFF':5},
          'DRMI_PRCL':           {'ON':-100, 'OFF':-100},
          'DRMI_PRCL_FMs':       {'ON':7,  'OFF':2},  #20241217 TJS SED changed from 3.5 & 1
          'DRMI_SRCL_noarms':    {'ON':10,   'OFF':5}, # Jul 26 2018, was 10, 5, Aug 5 2018, was 1, 2,
          'DRMI_SRCL_als':       {'ON':37,   'OFF':18}, #20241217 TJS SED put back the 37&18 from 20&10 #Old values 37 and 18 lowered for H1 locking issues Jun2024 cg # 20180926: was 30,15 # Jul 26 2018, was 20, 10, Aug 5 2018, was 10, 1, Aug 22 2018, was 20, 15, 25 
          'DRMI_SRCL_fromPRMI':  {'ON':48,  'OFF':5}, # 20180926: was 40,15 # Aug 22 2018, was 35, 15, 25
          'DRMI_SRCL_als_FMs':   {'ON':5.5,  'OFF':1}, #{'ON':40,  'OFF':18},#20181211 Dan+Rana # 20180926: was 40,15  # Aug 5 2018, was 5.5, 1, Aug 22 2018, was 20, 15, 25
          'DRMI_SRCL_noarms_FMs':{'ON':10,  'OFF':5}, # Aug 5 2018, was 5.5, 1
          'DRMI_ASC':            {'ON':0.001, 'CAUTION':0.001}, # Need to be updated for AS_C_NSUM
          'DARM':                {'ON': -100,  'OFF': -100},
          'ARMs_Green':          {'QUIET': 0.01, 'LOCKED': 0.8}, # LOCKED was 0.85; 08/19/2018
          'Override':            {'ON': -10000, 'OFF': -10000},
          'LOCKLOSS_CHECK_DRMI': {'ON': 12, 'OFF': 10},    # Reduced by 2x 23June2022 JCD HAM1
          'LOCKLOSS_CHECK':      {'ON': 350, 'OFF': 325},  # Reduced by 2x 23June2022 JCD HAM1
          'MICHDARK':            {'LOCKED': 2000},
          'MICHBRIGHT':          {'LOCKED': 4000}, #CHANGED FROM 1000 GLM DDB 20181123
          'PRXY':                {'LOCKED': 1, 'OSCILLATING': 6}, # Locked thresh to 0.5 from 1.0, for bad PRM alignment. JCD 6Aug2019, alog 51077
          'SRMI_MICH':           {'ON':1000, 'OFF':400},
          'SRMI_SRCL':           {'ON':-1000, 'OFF':-1000},
         }

# trigger masks
trig_mask = {'MICH':[2],
             'PRCL':[2],
             'SRCL':[]}

trigDelay = {'PRCL_FM':0.2,
             'MICH_FM':0.1,
             'SRCL_FM':0.25}

#FMtrigDelaySRCL = 0.25 # was 0.25 before 20180922

lsc_mtrx_elems = {'MICH':4,
                  'PRCL':1.75, # was 3.5 (GV 2018-09-27),
                  'SRCL':4}

offset = {'SRCL_MODEHOP':-800,
          'SRCL_RETUNE':-191}  # updated 20240927  alog 80318


# asc_offset = {'SRC_AS72_P': -0.1,
#               'SRC_AS72_Y': 0.83}  # AS72 offsets for DRMI ASC and fullIFO, vals from alog 78332. JCD 11June2024
#asc_offset = {'SRC_AS72_P': -0.185,
#              'SRC_AS72_Y': -0.113}  # AS72 offsets for DRMI ASC and fullIFO, vals from JLW 13thJune2024.

asc_offset = {'SRC_AS72_P': -0.0417,
              'SRC_AS72_Y': 0.0977}  # AS72 offsets for DRMI ASC and fullIFO, vals from alog 78776. JLW 1stJuly2024.

# PD gain settings during acquisition
#SED wants to remove LSC REFL9 from this so that REFL A +B can be in the same units
pd_gain = {
    'LSC': {
        'REFLAIR': {
            '9':  0.25,
            '45': 1,
        }, # close PD
        'POPAIR': {
            '9':  1,
            '18': 2,
            '45': 1,
            '90': 4,
        }, # close PD
        'REFL': {
            '45': 0.275,
        }, # close PD
        'POP': {
            '9':  8,
            '45': 1,
        }, # close PD
    }, # close LSC
    'ASC': {
        'REFL': {
            '9':  1,
            '45': 1,
        }, # close PD
        'POP': {
            'X': 2.8,
        }, # close PD
        'AS': {
            '36': 2.8,
            '45': 1,
            '72': 1,
        }, # close PD
    }, # close ASC
} # close full dict


power_up_on_RF = False   #was True for 10W/pressure spike issues Jun2024 cg
use_reflair = False
reflair_relative_gain = 23  # we need 23dB more alanog gain if we are using REFL air than using REFL (lho alog 44378), and the same polarity

# AS Camera nominal exposure
camera_exp = {'ASAIR': 10000}

# VIOLIN damping settings
# list of filter modules to turn on, and gain, and drive dof (pit=default)
vio_settings = {
    # open ITMX
    'ITMX': {
#        '1': {
#            'FMs':['FM1', 'FM3', 'FM4'],
#            'gain': 0.5,
#            'drive': 'Y',
#            'max_gain': 2, # didn't ring up but didn't damp after 20 minutes at 4 on Y
#        }, #close mode    #This remains commented out because we are now damping it along with mode 9, using mode 9's filter.
        '2': {
            'FMs':  ['FM5','FM9','FM10'],
            'gain': -15,
            'drive': ['P'],
            'max_gain': -15, # close mode       # Mode2 adjusted, 11Mar2019, Kara and Jenne.
        },
        '3': {
            'FMs':  ['FM1', 'FM8', 'FM10'],#
            'gain': -40,#nominal gain =  -40 RAHUL 17 MARCH 2020
            'drive': ['P'],
            'max_gain': -40,#MAX gain =  -40 RAHUL 17 MARCH 2020

        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM2','FM10'],
            'gain': 1,
            'drive': ['Y'],
            'max_gain': 1,
        }, # close mode       # Mode4 adjusted, 11Mar2019, Kara and Jenne.
        '5': {
            'FMs':  ['FM1', 'FM2', 'FM10'],
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode
        '7': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 1,
            'drive': ['P'],
            'max_gain': 10,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM3','FM10'], # VS changed 'FM2' to FM3 17 Mar 2022
            'gain': -4,
            'drive': ['P'],
            'max_gain': -4,
        }, # close mode
         '9': {
             'FMs':  ['FM6','FM10'], # EMC changed to FM6 and FM10 on 19 May 2022
             'gain': 1,
             'drive': ['Y'],
             'max_gain': 1,
         }, # close mode
        '11': {# 995.177
            'FMs':  ['FM1', 'FM10'], # Added FM2 on 20May2022 JCD
            'gain': -2, #0.1, # Lowered to 30 from 200 when add FM2, 20May2022 JCD
            'drive': ['P'],
            'max_gain': -10,
            '2W': {
                'FMs':  ['FM1', 'FM2', 'FM4', 'FM10'],
                'gain': 1,
                'drive': ['P'],
                'max_gain': 1,
            },#close 2W settings
        }, # close mode
        '12': {# 995.462
            'FMs':  ['FM1', 'FM6', 'FM10'], # Remove FM4, add FM5+FM6 20May2022 JCD
                'gain': +1, # Set to +1 from -2 on 12/30 RC
            'drive': ['P'],
            'max_gain': +1,
        }, # close mode
        '13': {# 998.083
            'FMs':  ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
            'gain': 0.2, #3,
            'drive': ['P'],
            'max_gain': 0.2, #3,
        }, # close mode
        '14': {# 1001.843
            'FMs':  ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
            'gain': 20,
            'drive': ['P'],
            'max_gain': 20,
            '2W': {
                'FMs':  ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
                'gain': 500,
                'drive': ['P'],
                'max_gain': 500,
            },
        }, # close mode
        '15': {# 1001.940
            'FMs':  ['FM1', 'FM2', 'FM6', 'FM10'], # 150 deg
            'gain': 40, #400,
            'drive': ['P'],
            'max_gain': 40, #400,
        }, # close mode
        '16': {# 1002.859
            'FMs':  ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 30, #300,
            'drive': ['P'],
            'max_gain': 30, #300,
        }, # close mode
        '17': {# 1003.120
            'FMs':  ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 12, #120,
            'drive': ['P'],
            'max_gain': 12, #120,
        }, # close mode
        '19': { # 998.018  #this should be functional, but still needs testing
            'FMs': ['FM1', 'FM5', 'FM6', 'FM10'], 
            'gain': -1.5, # used to be 20 but this mode is causing us trouble
            'drive': ['Y'],
            'max_gain': -1.5, # New settings 05/12/23
        }, # close mode
    }, # close ITMX

    # open ITMY
    'ITMY': {
        '1': {
            'FMs':  ['FM1', 'FM10'],
            'gain': -10, # was -1, changed to -10 01/12/23
            'drive': ['P'],
            'max_gain': -10, # was -1
        }, # close mode
        '2': {
            'FMs':  ['FM1','FM2', 'FM10'], # RK 23 May 2022
            'gain': 10,
            'drive': ['P'],
            'max_gain': 30, # decreased from 40 by EMC on 23 May 2022
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM5', 'FM10'],
            'gain': 2,
            'drive': ['Y'],
            'max_gain': 2,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 0.4, #changed 6/5 AJ alog 70144
            'drive': ['Y'],
            'max_gain': 0.4,
        }, # close mode
        '5': {
            'FMs':  ['FM5','FM6', 'FM8', 'FM10'], #removed FM7 added FM8 +60 degrees RyanC 11/28/24
            'gain': 0.01, # set 11/28, Use this to damp 5 and 6. Was 0.02
            'drive': ['Y'],
            'max_gain': 0.01, #10, #0.03,
        }, # close mode
        '6': {
            'FMs':  ['FM1','FM2','FM10'], # this mode damped by settings in mode 5
            'gain': 0, #0.2, #-0.2 CMC trying new values alog62602,
            'drive': ['Y'],
            'max_gain': 0, #0.2,
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM10'], #removed FM2 as the mode was growing RK 17May 2023
            'gain': 0.2, #changed to 0 due to ringups at 0.3, EMC 20221112
            'drive': ['Y'],
            'max_gain': 0.2, #0.17,
        }, # close mode
        '8': {
            'FMs':  ['FM1','FM5','FM10'], # Changed FM3 to FM5 RyanC 12/08
            'gain': 0.1 , #changed to -0.2 AJ, 6/5 alog 70144, changed to +0.1 on 12/08 RyanC
            'drive': ['Y'],
            'max_gain': 0.3,
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '11': {
            'FMs': ['FM1', 'FM2', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '12': {
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '13': {
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '14': {
            'FMs': ['FM1', 'FM4', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 50,
        }, # close mode
        '15': {
            'FMs': ['FM1', 'FM5', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '16': {
            'FMs': ['FM1', 'FM6', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '17': {
            'FMs': ['FM1', 'FM6', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
#        '18': { # 998.825 # this is being damped in guardian for the firs time with a new filter as of 11sep2020,CV
#            'FMs': ['FM1', 'FM2', 'FM10'], # 180 deg
#            'gain': 30, # 11sep2020, new filter gain = 30, CV
#            'drive': ['P'],
#            'max_gain': 30,# 11sep2020, new filter gain = 30 CV
#        }, # close mode
    }, # close ITMY

    # open ETMX
    'ETMX': {
        '2': {
            'FMs':  ['FM1','FM2','FM10'],
            'gain': -20, # -0.2
            'drive': ['Y'],
            'max_gain': -20,
            '2W': {
                'FMs':  ['FM1', 'FM2', 'FM10'],
                'gain': -20,
                'drive': ['Y'],
                'max_gain': -20,
            },#close 2W settings            
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM3','FM10'],
            'gain': 15,
            'drive': ['P'],
            'max_gain': 15,
            '2W': {
                'FMs':  ['FM1', 'FM3', 'FM10'],
                'gain': 15,
                'drive': ['P'],
                'max_gain': 15,
            },#close 2W settings
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 20,
            'drive': ['P'],
            'max_gain': 20,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 20,
                'drive': ['P'],
                'max_gain': 20,
            },#close 2W settings
        }, # close mode
        '6': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 5,
                'drive': ['Y'],
                'max_gain': 5,
            },#close 2W settings
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM2', 'FM10'],
            'gain': 3,
            'drive': ['Y'],
            'max_gain': 3,
            '2W': {
                'FMs':  ['FM1', 'FM2', 'FM10'],
                'gain': 3,
                'drive': ['Y'],
                'max_gain': 3,
            },#close 2W settings
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM2', 'FM10'],
            'gain': -10,
            'drive': ['P'],
            'max_gain': -10,
            '2W': {
                'FMs':  ['FM1', 'FM2', 'FM10'],
                'gain': -10,
                'drive': ['P'],
                'max_gain': -10,
            },#close 2W settings
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM10'],
            'gain': -6,
            'drive': ['Y'],
            'max_gain': -8,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': -6,
                'drive': ['Y'],
                'max_gain': -8,
            },#close 2W settings
        }, # close mode


        '11': { # 1006.439
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 8, # 30
            'drive': ['P'],
            'max_gain': 8, # 30
            '2W': {
                'FMs':  ['FM1', 'FM6', 'FM10'],
                'gain': 8,
                'drive': ['P'],
                'max_gain': 8,
            },#close 2W settings
        }, # close mode
        '12': { # 1006.747
            'FMs': ['FM1', 'FM10'], # 0 deg
            'gain': 50, # was 100
            'drive': ['P'],
            'max_gain': 50, # was 100
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 50,
                'drive': ['P'],
                'max_gain': 50,
            },#close 2W settings
        }, # close mode
        '13': { # 1010.356
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 30,
            'drive': ['P'],
            'max_gain': 30,
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM10'],
                'gain': 30,
                'drive': ['P'],
                'max_gain': 30,
            },#close 2W settings            
        }, # close mode
        '14': { # 1010.581
            'FMs': ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
            'gain': 50, # was 500
            'drive': ['P'],
            'max_gain': 50, # was 500
            '2W': {
                'FMs':  ['FM1', 'FM3', 'FM5', 'FM10'],
                'gain': 50,
                'drive': ['P'],
                'max_gain': 50,
            },#close 2W settings            
        }, # close mode
        '15': { # 1011.063
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 25,
            'drive': ['P'],
            'max_gain': 25,
            '2W': {
                'FMs':  ['FM1', 'FM6', 'FM10'],
                'gain': 25,
                'drive': ['P'],
                'max_gain': 25,
            },#close 2W settings
        }, # close mode
        '16': { # 1013.869
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 30,
            'drive': ['P'],
            'max_gain': 30,
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM10'],
                'gain': 30,
                'drive': ['P'],
                'max_gain': 30,
            },#close 2W settings            
        }, # close mode
        '17': { # 1014.091
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 50, # was 200
            'drive': ['P'],
            'max_gain': 50, # was 200
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM6', 'FM10'],
                'gain': 50,
                'drive': ['P'],
                'max_gain': 50,
            },#close 2W settings
        }, # close mode
#        '18': { # 1011.336  #commented out for more testing
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 2000, #could go up to 300
#            'drive': ['P'],
#            'max_gain': 2000,
#        }, # close mode
    }, # close ETMX

    # open ETMY  2W settings added 02/28, modes 1&20 were set to zero due to sign changes and more testing needed
    'ETMY': {
        '1': {
            'FMs':  ['FM1', 'FM10'], #['FM1','FM8','FM10'],
            'gain': -0.2, # nominal 0.1 11/24
            'drive': ['Y'],
            'max_gain': -0.2, # 0.2, setting to 0, 4/14
            '2W': {
                'FMs':  ['FM1', 'FM7', 'FM10'], # ['FM1', 'FM6', 'FM8', 'FM10']
                'gain': 0,
                'drive': ['Y'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '2': {
            'FMs':  ['FM1','FM3','FM10'],
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
            '2W': {
                'FMs':  ['FM1', 'FM3', 'FM10'],
                'gain': -2,
                'drive': ['Y'],
                'max_gain': -2,
            },#close 2W settings    
            }, # Close mode settings
        '3': {
            'FMs':  ['FM1','FM3', 'FM10'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
            '2W': {
                'FMs':  ['FM1', 'FM3', 'FM10'],
                'gain': 5,
                'drive': ['Y'],
                'max_gain': 5,
            },#close 2W settings    
            }, # Close mode settings
        '4': { #checked glm 220316
            'FMs':  ['FM1', 'FM10'],
            'gain': -3.0,
            'drive': ['Y'],
            'max_gain': -3,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': -3,
                'drive': ['Y'],
                'max_gain': -3,
            },#close 2W settings    
            }, # Close mode settings
        '5': {
            'FMs':  ['FM1', 'FM10'],
            'gain': 0.2,
            'drive': ['Y'],
            'max_gain': 0.2,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 0.2,
                'drive': ['Y'],
                'max_gain': 0.2,
            },#close 2W settings    
            }, # Close mode settings
        '6': {
            'FMs':  ['FM1', 'FM7','FM10'],
            'gain': 0.0, # VS added on 19Apr2021;
            'drive': ['Y'],
            'max_gain': 0.0,
            '2W': {
                'FMs':  ['FM1', 'FM7', 'FM10'],
                'gain': 0,
                'drive': ['Y'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '7': {
            'FMs':  ['FM1', 'FM9', 'FM10'], # moved 100dB from FM4 to FM10, CV, 13aug2020
            'gain': 25, #25 works as of 05/04/230 -doesnt effect mode8
            'drive': ['P'],
            'max_gain': 25, #25
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 0,
                'drive': ['P'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '8': {
            'FMs':  ['FM1', 'FM5', 'FM10'], # VS 18Mar2020
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
            '2W': {
                'FMs':  ['FM1', 'FM5', 'FM10'],
                'gain': -2,
                'drive': ['Y'],
                'max_gain': -2,
            },#close 2W settings    
            }, # Close mode settings
        '9': {
            'FMs':  ['FM2'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
            '2W': {
                'FMs':  ['FM2'],
                'gain': 0,
                'drive': ['P'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '10': {
            'FMs':  ['FM1', 'FM10'], # This is a diff version of 510Hz filters, for combo mode1+6 damping
            'gain': 0,
            'drive': ['Y'],
            'max_gain': 0,
            '2W': {
                'FMs':  ['FM1', 'FM10'],
                'gain': 0,
                'drive': ['Y'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '11': { # 1000.061
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 20, # set to 20, from 50, 10Aug2020, CV, rkumar
            'drive': ['P'],
            'max_gain': 20,
            '2W': {
                'FMs':  ['FM1', 'FM6', 'FM10'],
                'gain': 20,
                'drive': ['P'],
                'max_gain': 20,
            },#close 2W settings    
            }, # Close mode settings
        '12': { # 1000.423, new damping filter in FM7, as of 6Aug2020, CV, rkumar
            'FMs': ['FM7', 'FM10'],
            'gain': 6,
            'drive': ['P'],
            'max_gain': 6,
            '2W': {
                'FMs':  ['FM7', 'FM10'],
                'gain': 6,
                'drive': ['P'],
                'max_gain': 6,
            },#close 2W settings    
            }, # Close mode settings
        '13': { # 1009.932
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 50,
            'drive': ['P'],
            'max_gain': 50,
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM10'],
                'gain': 50,
                'drive': ['P'],
                'max_gain': 50,
            },#close 2W settings    
            }, # Close mode settings
        '14': { # 1017.985
            'FMs': ['FM1', 'FM2', 'FM6', 'FM10'], # 150 deg
            'gain': 20,
            'drive': ['P'],
            'max_gain':20,
            '2W': {
                'FMs':  ['FM1', 'FM2', 'FM6', 'FM10'],
                'gain': 20,
                'drive': ['P'],
                'max_gain': 20,
            },#close 2W settings    
            }, # Close mode settings
        '15': { # 1018.238
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 50, # was 150
            'drive': ['P'],
            'max_gain': 50,
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM6', 'FM10'],
                'gain': 50,
                'drive': ['P'],
                'max_gain': 50,
            },#close 2W settings    
            }, # Close mode settings
        '18': { # 1000.294
            'FMs': ['FM1', 'FM4', 'FM10'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM10'],
                'gain': 0,
                'drive': ['P'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
        '20': { # 1000.307, old notes prior to 10Aug2020 removed
            'FMs': ['FM1', 'FM2', 'FM10'], # FM4 to FM2 Jan 2025
            'gain': -1, # Changed from -1 to 0 as it was ringing up 01/25/25 RyanC, it was 0.1 in previous years
            'drive': ['Y'],
            'max_gain': -1,  # 0.1 after seeing it work for a few locks 3/27
            '2W': {
                'FMs':  ['FM1', 'FM4', 'FM10'],
                'gain': 0, # Theres a sign change somewhere in the power so leaving it at 0
                'drive': ['Y'],
                'max_gain': 0,
            },#close 2W settings    
            }, # Close mode settings
    }, # close ETMY
} # close dict

vio_mon = {  #monitor levels for which the guardian should do certain things
    'RF_noise_floor':3.0, # this is the noise floor when
    'DC_noise_floor':1.0,
    'Rung_up':5.5,# this mode is rung up so higfh we should engage with low gain
    'Nominal':5}
