from guardian import GuardState, GuardStateDecorator, NodeManager
import re
from ISC_library import unstall_nodes

"""The General

Manage the SEI Config nodes. Have simple states that can be easily figured out.

We need to somehow let the operators know what states use the BRS.
"""
########################################
# Managed nodes
#ham_sc_nodes = ['ISI_HAM2_SC', 'ISI_HAM3_SC', 'ISI_HAM4_SC', 'ISI_HAM5_SC', 'ISI_HAM6_SC']
ham_sc_nodes = ['SEI_CS']
hpi_sc_nodes = ['HPI_ETMX_SC', 'HPI_ETMY_SC']
bsc_sc_nodes = ['ISI_ETMX_ST1_SC',
                'SEI_CS',
                'ISI_ETMY_ST1_SC',
                'ISI_ETMX_ST2_SC',
                'ISI_ETMY_ST2_SC',
                'ISI_ITMX_ST2_SC',
                'ISI_ITMY_ST2_SC',
                'ISI_BS_ST2_SC',
]

bsc_st1_nodes = ['ISI_ETMX_ST1_SC',
                'ISI_ETMY_ST1_SC',
                'SEI_CS',
]

bsc_st2_nodes = ['ISI_ETMX_ST2_SC',
                'ISI_ETMY_ST2_SC',
                'ISI_ITMX_ST2_SC',
                'ISI_ITMY_ST2_SC',
                'ISI_BS_ST2_SC',
]

bsc_blend_nodes = ['ISI_ETMX_ST1_BLND',
                'ISI_ETMY_ST1_BLND',
                'ISI_ITMX_ST1_BLND',
                'ISI_ITMY_ST1_BLND',
                'ISI_BS_ST1_BLND',
                'ISI_ETMX_ST2_BLND',
                'ISI_ETMY_ST2_BLND',
                'ISI_ITMX_ST2_BLND',
                'ISI_ITMY_ST2_BLND',
                'ISI_BS_ST2_BLND',
]

ham_blend_nodes = ['ISI_HAM2_BLND',
                'ISI_HAM3_BLND',
                'ISI_HAM4_BLND',
                'ISI_HAM5_BLND',
                'ISI_HAM6_BLND',
                'ISI_HAM7_BLND',
                'ISI_HAM8_BLND',
]


node_list = ham_sc_nodes + bsc_sc_nodes + bsc_blend_nodes + hpi_sc_nodes + ham_blend_nodes
nodes = NodeManager(node_list)

########################################
# Constants
BSCS = ['BS','ITMX','ITMY','ETMX','ETMY']
HAMS = ['HAM1', 'HAM2','HAM3','HAM4','HAM5','HAM6','HAM7','HAM8']

bsc_match_gain_chans_gen = ['HPI-{}_SENSCOR_Z_MATCH_GAIN',
                            'ISI-{}_ST1_SENSCOR_X_MATCH_GAIN',
                            'ISI-{}_ST1_SENSCOR_Y_MATCH_GAIN',
                            'ISI-{}_ST2_SENSCOR_X_MATCH_GAIN',
                            'ISI-{}_ST2_SENSCOR_Y_MATCH_GAIN'
]
bsc_match_gain_chans = [gen_chan.format(bsc) for bsc in BSCS \
                             for gen_chan in bsc_match_gain_chans_gen]

bsc_match_gain_chans_gen = ['HPI-{}_SENSCOR_Z_MATCH_TRAMP',
                            'ISI-{}_ST1_SENSCOR_X_MATCH_TRAMP',
                            'ISI-{}_ST1_SENSCOR_Y_MATCH_TRAMP',
                            'ISI-{}_ST2_SENSCOR_X_MATCH_TRAMP',
                            'ISI-{}_ST2_SENSCOR_Y_MATCH_TRAMP'
]
bsc_match_tramp = [gen_chan.format(bsc) for bsc in BSCS \
                             for gen_chan in bsc_match_gain_chans_gen]

ham_match_gain_chans = []
ham_match_tramp = []
for ham in HAMS:
    for dof in ['X', 'Y', 'Z']:
        if ham == 'HAM1':
            ham_match_gain_chans.append('HPI-HAM1_SENSCOR_{}_MATCH_GAIN'.format(dof))
            ham_match_gain_chans.append('HPI-HAM1_3DL4C_FF_{}_GAIN'.format(dof))            
            ham_match_tramp.append('HPI-HAM1_SENSCOR_{}_MATCH_TRAMP'.format(dof))
        else:
            ham_match_gain_chans.append('ISI-{}_SENSCOR_{}_MATCH_GAIN'.format(ham, dof))
            ham_match_tramp.append('ISI-{}_SENSCOR_{}_MATCH_TRAMP'.format(ham, dof))


# len = 43 chans as of Sept 11, 2019
all_match_gain_chans = bsc_match_gain_chans + ham_match_gain_chans
all_match_tramp = bsc_match_tramp + ham_match_tramp
########################################
# Tools

def toggle_other_sc(on_or_off):
    """Use this function to recover or turn off
    the others bits used for SC that are not coved by
    the suboordinate nodes.

    on_or_off  -  Should be a string of either 'on' or 'off'
    """
    assert on_or_off == 'on' or on_or_off == 'off', \
        "\'{}\' should be either \'on\' or \'off\'".format(on_or_off)

    if on_or_off == 'on':
        gain = 1
    else:
        gain = 0

    for chan in all_match_tramp:
        ezca[chan] = 5

    for chan in all_match_gain_chans:
        ezca[chan] = gain


class check_match_gains_engaged(GuardStateDecorator):
    """Decorator to check on engaged match banks.
    Usage example:

    class WINDY(GuardState):

        index = 40
        goto = False
        request = True
        def main(self):
            code
            code
            code

        @nodes.checker()
        @unstall_nodes(nodes)
    --> @check_match_gains_engaged
        def run(self):
            code
            code
            code
    """
    def pre_exec(self):
        for chan in all_match_gain_chans:
            if ezca[chan] == 0:
                notify('SC match gain(s) not engaged. Request RECOVER_SC')

########################################
# States

nominal='WINDY'

"""How can we make INIT look at the different suboordinate configs 
and go to that state, withough editing INIT, but adding states???
"""

# FIXME: How can we do this????
class INIT(GuardState):
    """This state should be able to determine where in the state graph
    it should be and then go there or reset to a plausible spot.
    """
    def main(self):
        nodes.set_managed()
        # Check for known configs...somehow
        
    def run(self):
        return 'HOME'

    
class HOME(GuardState):
    """The central spoke. All nodes that do not have to turn off SC
    should go to and from this state. This should be an idle state.
    """
    def main(self):
        return True
    def run(self):
        return True

    
class RECOVER_SC(GuardState):
    """
    """
    index = 15
    goto = True
    request = False
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'
        toggle_other_sc('on')
        return True
    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True


class SC_OFF_NOBRSXY(GuardState):
    """
    """
    index = 10
    goto = True
    request = True
    def main(self):
        # SC OFF
        for node in bsc_st2_nodes:
            nodes[node] = 'CONFIG_FIR'
        for node in ham_sc_nodes + bsc_st1_nodes:
            nodes[node] = 'CONFIG_SC_OFF'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_OFF'
        toggle_other_sc('off')
        # BSC HEPI Z
        '''
        for bsc in ['BS','ITMX','ITMY','ETMX','ETMY']:
            ezca['HPI-{}_SENSCOR_Z_MATCH_GAIN'.format(bsc)] = 0
        # HAM ISI Z
        for ham in ['HAM2','HAM3','HAM4','HAM5','HAM6']:
            ezca['ISI-{}_SENSCOR_Z_MATCH_GAIN'.format(ham)] = 0
        # FIXME: But what about the other HAM HEPIs????
        # HAM1 HEPI XYZ
        for dof in ['X','Y','Z']:
            ezca['HPI-HAM1_SENSCOR_{}_MATCH_GAIN'.format(dof)] = 0
        '''
    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

        
class LARGE_EQ_NOBRSXY(GuardState):
    """Move suboordinates to their earthquake config

    """
    index = 17
    goto = True
    request = True
    def main(self):
        # All SC off
        for node in bsc_st2_nodes:
            nodes[node] = 'SC_OFF'
        for node in ham_sc_nodes + bsc_st1_nodes:
            nodes[node] = 'CONFIG_SC_OFF'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_OFF'


        toggle_other_sc('off')
        
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_HIGH_BLEND'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_HIGH_BLEND'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_HIGH_BLEND'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_HIGH_BLEND'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_HIGH_BLEND'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_HIGH_BLENDS'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_HIGH_BLENDS'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

        
class WINDY(GuardState):
    """Move suboordinates to their windy config

    """
    index = 40
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'
    
    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True
 
class MORE_WINDY(GuardState):
    """Move suboordinates to their windy config

    """
    index = 43
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True



class WINDY_NO_BRSY(GuardState):
    """Move suboordinates to their windy config

    """
    index = 60
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

        
class WINDY_NO_BRSX(GuardState):
    """Move suboordinates to their windy config

    """
    index = 65
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

class NOBRSXY_WINDY(GuardState):
    """Very high wind, low microseism state

    """
    index = 45
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

'''
class USEISM_NOBRSXY(GuardState):    
    """Move suboordinates to their useism config
    
    """
    index = 30
    goto = False
    request = False
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'BLEND_45MHZ'
        nodes['ISI_ITMX_ST1_BLND'] = 'BLEND_45MHZ'
        nodes['ISI_ITMY_ST1_BLND'] = 'BLEND_45MHZ'
        nodes['ISI_ETMX_ST1_BLND'] = 'BLEND_45MHZ'
        nodes['ISI_ETMY_ST1_BLND'] = 'BLEND_45MHZ'
        nodes['ISI_ETMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'BLEND_250MHZ'
        # SC
        nodes['ISI_BS_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ITMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ITMY_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True
'''
        
class USEISM(GuardState):    
    """Move suboordinates to their useism config
    
    """
    index = 31
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_SWARM'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_M102'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

class NOBRSXY_USEISM(GuardState):    
    """Move suboordinates to their useism config
    
    """
    index = 32
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_SWARM'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_M102'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

class USEISM_EARTHQUAKE(GuardState):    
    """Move suboordinates to their useism config
    
    """
    index = 18
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_EQ'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_EQ'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_XYZ105'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_XYZ105'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_XYZ105'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_XYZ105'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_XYZ105'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_M102'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_M102'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_EQ'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_EQ'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True


        
class USEISM_WINDY(GuardState):    
    """Move suboordinates to their windy, useism config
    
    """
    index = 36
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_mH105'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

'''       
class CALM_NOBRSXY(GuardState):
    """Low wind, low microseism, no brs configuration

    """
    index = 20
    goto = False
    request = False
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_WINDY'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'BLEND_QUITE90'
        nodes['ISI_ITMX_ST1_BLND'] = 'BLEND_QUITE90'
        nodes['ISI_ITMY_ST1_BLND'] = 'BLEND_QUITE90'
        nodes['ISI_ETMX_ST1_BLND'] = 'BLEND_QUITE90'
        nodes['ISI_ETMY_ST1_BLND'] = 'BLEND_QUITE90'
        nodes['ISI_ETMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'BLEND_250MHZ'
        # SC
        nodes['ISI_BS_ST1_SC'] = 'CONFIG_WNR'
        nodes['ISI_ITMX_ST1_SC'] = 'CONFIG_WNR'
        nodes['ISI_ITMY_ST1_SC'] = 'CONFIG_WNR'
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WNR'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WNR'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True
'''
        
class EARTH_QUAKE(GuardState):
    """Move suboordinates to their earthquake config

    """
    index = 19
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_EQ'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_EQ'

        #BSCs
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_XYZ250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_EQ'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_EQ'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

class TEST_SWARM(GuardState):
    """Swarm test config for Oct 2019

    """
    index = 101
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_SWARM'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True

class UGLY(GuardState):
    """Swarm test config for Oct 2019

    """
    index = 66
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_SWARM'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_SC_ON'

        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'CONFIG_QUITE90'
        nodes['ISI_ETMY_ST1_BLND'] = 'CONFIG_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'CONFIG_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM7_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM8_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_SC_OFF'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'

    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True
'''
class CENTRAL(GuardState):
    """Move suboordinates to their central config

    """
    index = 50
    goto = False
    request = True
    def main(self):
        # HAMs SC ON
        for ham_node in ham_sc_nodes:
            nodes[ham_node] = 'CONFIG_CENTRAL'
        for hpi_node in hpi_sc_nodes:
            nodes[hpi_node] = 'CONFIG_CENTRAL'
        # Blends
        nodes['ISI_BS_ST1_BLND'] = 'BLEND_QUITE250'
        nodes['ISI_ITMX_ST1_BLND'] = 'BLEND_QUITE250'
        nodes['ISI_ITMY_ST1_BLND'] = 'BLEND_QUITE250'
        nodes['ISI_ETMX_ST1_BLND'] = 'BLEND_QUITE250'
        nodes['ISI_ETMY_ST1_BLND'] = 'BLEND_QUITE250'
        nodes['ISI_ETMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ETMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMY_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_ITMX_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_BS_ST2_BLND'] = 'BLEND_250MHZ'
        nodes['ISI_HAM2_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM3_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM4_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM5_BLND'] = 'CONFIG_COMP250'
        nodes['ISI_HAM6_BLND'] = 'CONFIG_COMP250'
        # SC
        nodes['ISI_BS_ST1_SC'] = 'CONFIG_CENTRAL'
        nodes['ISI_ITMX_ST1_SC'] = 'CONFIG_CENTRAL'
        nodes['ISI_ITMY_ST1_SC'] = 'CONFIG_CENTRAL'
        nodes['ISI_ETMX_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST1_SC'] = 'CONFIG_WINDY'
        nodes['ISI_ETMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ETMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMY_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_ITMX_ST2_SC'] = 'CONFIG_FIR'
        nodes['ISI_BS_ST2_SC'] = 'SC_OFF'
    
    @nodes.checker()
    @unstall_nodes(nodes)
    @check_match_gains_engaged
    def run(self):
        if not nodes.arrived:
            notify('Not all nodes arrived')
        else:
            return True
'''
########################################
# Edges

edges = [
    # From HOME should be every state that does not turn off SC
    ('HOME', 'WINDY'),
    ('HOME', 'MORE_WINDY'),
    ('HOME', 'WINDY_NO_BRSY'),
    ('HOME', 'WINDY_NO_BRSX'),
    ('HOME', 'NOBRSXY_WINDY'),
    ('HOME', 'NOBRSXY_USEISM'),    
#    ('HOME', 'USEISM_NOBRSXY'),
    ('HOME', 'USEISM_WINDY'),
#    ('HOME', 'CALM_NOBRSXY'),
    ('HOME', 'EARTH_QUAKE'),
    ('HOME', 'USEISM'),
    ('HOME', 'USEISM_EARTHQUAKE'),
    ('HOME', 'TEST_SWARM'),
    ('HOME', 'UGLY'),
    ('SC_OFF_NOBRSXY', 'RECOVER_SC'),
    ('LARGE_EQ_NOBRSXY', 'RECOVER_SC'),
    ('RECOVER_SC', 'HOME'),
]
